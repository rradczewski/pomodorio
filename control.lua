require("mod-gui")

next_pause = nil
labelStyle = {rich_text_setting = defines.rich_text_setting.enabled}

function create_gui(player)
    local flow = mod_gui.get_frame_flow(player)
    local frame = flow.add{
        type = "frame",
        name = "pomodorio-timer-frame",
        style = mod_gui.frame_style
    }

    timeLabel = frame.add{
        type = "label",
        name = "pomodorio-timer-time",
        caption = "[color=red][font=default-bold]00:00[/font][/color]"
    }
    timeLabel.style.rich_text_setting = defines.rich_text_setting.enabled
    timeLabel.style.horizontal_align = "center"

    local buttonMinus = flow.add{
        type = "button",
        name = "pomodorio-minus",
        caption = "-",
        style = mod_gui.button_style
    }
    local buttonPlus = flow.add{
        type = "button",
        name = "pomodorio-plus",
        caption = "+",
        style = mod_gui.button_style
    }
    local buttonResume = flow.add{
        type = "button",
        name = "pomodorio-resume",
        caption = "Resume",
        style = mod_gui.button_style
    }
end

function update_clock(tick)
    if not timeLabel then return end
    if not next_pause then return end

    if next_pause < tick then
        timeLabel.caption = "[color=red][font=default-bold]00:00[/font][/color]"
        game.tick_paused = true
        return
    end

    local timeLeft = math.floor((next_pause - tick) / 60)

    local timeLeftMinutes = math.floor(timeLeft / 60)
    local timeLeftSeconds = timeLeft % 60

    timeLabel.caption = "[color=red][font=default-bold]" ..
                            string.format("%02d:%02d", timeLeftMinutes,
                                          timeLeftSeconds) .. "[/font][/color]"
end

script.on_event({defines.events.on_player_joined_game},
                function(e) create_gui(game.get_player(1)) end)

script.on_event({defines.events.on_tick}, function(e) update_clock(e.tick) end)

script.on_event({defines.events.on_gui_click}, function(e)
    game.print(e.element.name)
    if e.element.name == "pomodorio-plus" then
        if not next_pause then
            next_pause = e.tick + 60 * 60 * 25
        elseif next_pause > e.tick then
            -- Next pause is in the future
            next_pause = next_pause + 60 * 60
        else
            next_pause = e.tick + 60 * 60
        end
    end
    if e.element.name == "pomodorio-minus" then
        if not next_pause then
            next_pause = e.tick + 60 * 60 * 10
        elseif next_pause > (e.tick + 60 * 60) then
            -- Next pause is in the future
            next_pause = next_pause - 60 * 60
        elseif next_pause > (e.tick + 60) then
            -- Next pause is in the future
            next_pause = next_pause - 60 * 5
        else
            next_pause = nil
        end
    end
    if e.element.name == "pomodorio-resume" then
        game.tick_paused = false
        next_pause = e.tick + 60 * 60 * 25
    end
end)
